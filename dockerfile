FROM pytorch/pytorch:1.5.1-cuda10.1-cudnn7-runtime

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install -y curl python-dev g++ make

# /root/.nvm or /usr/local/nvm, depending
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 10.21.0

RUN mkdir $NVM_DIR

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN npm install -g yarn

RUN mkdir theia-ide 
COPY package.json /workspace/theia-ide/package.json
WORKDIR /workspace/theia-ide

COPY pytorch-tut /workspace/pytorch-tut

RUN yarn
RUN yarn theia build

EXPOSE 8080

ENTRYPOINT exec yarn start /workspace/pytorch-tut --hostname 0.0.0.0 --port 8080
